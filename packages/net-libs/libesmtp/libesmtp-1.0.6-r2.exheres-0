# Copyright 2009, 2010 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Library that implements the client side of the SMTP protocol"
HOMEPAGE="http://brianstafford.info/${PN}"
LICENCES="LGPL-2.1 GPL-2"
DESCRIPTION="
libESMTP is an SMTP client which manages posting (or submission of) electronic
mail via a preconfigured Mail Transport Agent (MTA). It may be used as part of a
Mail User Agent (MUA) or other program that must be able to post electronic mail
but where mail functionality is not that program's primary purpose.

The availability of a reliable lightweight thread-safe SMTP client eases the
task of coding for software authors thus improving the quality of the resulting
code."
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/api.html"

SLOT="0"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.bz2"
PLATFORMS="~amd64"
MYOPTIONS="
    debug
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"
DEPENDENCIES="
    providers:libressl? ( dev-libs/libressl:= )
    providers:openssl? ( dev-libs/openssl:=[>=0.9.6b&<1.1] )
"

# NTLMv1 is ancient and not recommended to use. Also it won't compile against libressl
# without patching (they removed lowercase variants for functions in des.h).
DEFAULT_SRC_CONFIGURE_PARAMS=(
    "--enable-all"
    "--disable-ntlm"
    "--with-openssl"
    "--hates=docdir"
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( "debug" )

DEFAULT_SRC_INSTALL_EXTRA_DOCS=("Notes" "doc/api.xml")

