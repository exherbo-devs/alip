# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Copyright 2024 Ali Polatel <alip@chesswob.org>
# Distributed under the terms of the GNU General Public License v2
#
# Based in part upon 'openrc-0.20.4.ebuild' from Gentoo, which is:
#     Copyright 1999-2015 Gentoo Foundation
#     Distributed under the terms of the GNU General Public License v2

require bash-completion zsh-completion
require github
require meson

SUMMARY="A dependency-based init system that works with the system-provided init program"
SLOT="0"
LICENCES="BSD-2"

MYOPTIONS="
    audit
    ( providers:
        runit
        sinit
        sysvinit
    ) [[
        *description = [ provider for the init daemon ]
        number-selected = at-most-one
    ]]
    ncurses
    newnet [[ description = [
            Enable the new network stack (experimental)
        ] ]]
    pam
    selinux
"

PLATFORMS="~amd64 ~x86"

DEPENDENCIES="
    build+run:
        audit? ( app-admin/audit )
        ncurses? ( sys-libs/ncurses )
        pam? ( sys-libs/pam )
        selinux? ( security/libselinux )
    run:
        app-shells/bash
        providers:runit? ( sys-apps/runit )
        providers:sinit? ( sys-apps/sinit )
        providers:sysvinit? ( sys-apps/sysvinit )
    recommendation:
        net-misc/netifrc [[ description = [
            Network Interface Management Scripts
        ] ]]
"

REMOTE_IDS="github:OpenRC/${PN}"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dos=Linux
    "-Dbranding=\"Exherbo Linux\""
    -Dshell=/bin/sh
    -Dpkgconfig=true
)

MESON_SRC_CONFIGURE_OPTIONS=(
    audit
    newnet
    pam
    'ncurses -Dtermcap=ncurses -Dtermcap=termcap'
    'bash-completion -Dbash-completions=true'
    'zsh-completion -Dzsh-completions=true'
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    selinux
)

pkg_setup() {
    exdirectory --allow /etc/sysctl.d
}

pkg_postinst() {
    elog "Since openrc 0.35 openrc has a cgroup init.d script which"
    elog "you'll have to add to your startup if you need cgroups."
}

