# Copyright 2009, 2011 Ali Polatel <alip@exherbo.org>
# Copyright 2014 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=lunarmodules tag=v${PV} ]
require lua [ multiunpack=true whitelist="5.1 5.2 5.3 5.4" ]

SUMMARY="Network support for the Lua language"
DESCRIPTION="
LuaSocket is a Lua extension library that is composed by two parts: a C core
that provides support for the TCP and UDP transport layers, and a set of Lua
modules that add support for functionality commonly needed by applications that
deal with the Internet.
"
REMOTE_IDS="luaforge:${PN}"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

# Tests seem to expect luasocket already installed
RESTRICT="test"

prepare_one_multibuild() {
    edo sed -e "s/ -O2 -ggdb3/ ${CFLAGS}/" \
            -e "s/CC_linux=gcc/CC_linux=${CC}/" \
            -e "s/LD_linux=gcc/LD_linux=${CC}/" \
            -i src/makefile
}

compile_one_multibuild() {
    emake PLAT=linux LUAV=$(lua_get_abi) prefix=/usr LUAINC=/usr/include/lua$(lua_get_abi)
}

install_one_multibuild() {
    emake -j1 DESTDIR="${IMAGE}" LUAV=${luav} LUAPREFIX_linux=/usr \
        CDIR_linux=$(exhost --target)/lib/lua/$(lua_get_abi) \
        LDIR_linux=share/lua/$(lua_get_abi) \
        install-unix
    emagicdocs
}

