# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic

SUMMARY="Extremely lightweight but extensible database access library"
DESCRIPTION="
OpenDBX is an extremely lightweight but extensible database access library
written in C. It provides an abstraction layer to all supported databases with a
single, clean and simple interface that leads to an elegant code design
automatically. If you want your application to support different databases with
little effort, this is definitively the right thing for you!
"
HOMEPAGE="http://www.linuxnetworks.de/doc/index.php/OpenDBX"
DOWNLOADS="http://linuxnetworks.de/opendbx/download/${PNV}.tar.gz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="( sqlite mysql postgresql ) [[ number-selected = at-least-one ]]"

DEPENDENCIES="
    build+run:
        sqlite? ( dev-db/sqlite )
        mysql? ( dev-db/mysql )
        postgresql-client? ( dev-db/postgresql )
"

pkg_setup() {
    if option mysql; then
        append-cppflags -I/usr/include/mysql
        append-ldflags -L/usr/lib/mysql
    fi
}

src_configure() {
    local backends=""

    option mysql && backends="${backends} mysql"
    option sqlite && backends="${backends} sqlite3"
    option postgresql && backends="${backends} pgsql"

    econf --with-backends="${backends}"
}

src_install() {
    default
    edo find "${IMAGE}"/usr/share -type d -empty -delete
}
