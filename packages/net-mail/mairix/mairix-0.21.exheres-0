# Copyright 2009 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ]

SUMMARY="Email index and search tool"
DESCRIPTION="
Mairix is a program for indexing and searching email messages stored in Maildir,
MH or mbox folders.
Some features:
Indexing is fast. It runs incrementally on new messages - any particular message
only gets scanned once in the lifetime of the index file.
The search mode populates a \"virtual\" maildir (or MH) folder with symlinks
which point to the real messages. This folder can be opened as usual in your
mail program. (Note, if messages are in mbox folders, copies are made.
Similarly if the virtual folder has mbox format, it is filled with copies of the
matched messages.)
The search mode is very fast.
Indexing and searching works on the basis of words. The index file tabulates
which words occur in which parts (particular headers + body) of which messages.
"
HOMEPAGE="http://www.rpcurnow.force9.co.uk/${PN}/"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        sys-devel/bison
        sys-devel/flex
    build+run:
        app-arch/bzip2
        sys-libs/zlib
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-gzip-mbox --enable-bzip-mbox )
DEFAULT_SRC_INSTALL_EXTRA_DOCS=( dotmairixrc.eg )

src_prepare() {
    default

    # Note to self, punch upstream for dying on unrecognized options!
    sed -i \
        -e "/^[[:space:]]*bad_options=yes/d" \
        configure || die "sed in configure failed"
}

